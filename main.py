from fastapi import FastAPI

# from client import Cliente
from app.archivos.files_csv import crear_dataframe_cliente
from typing import Optional

app = FastAPI()



@app.get("/search/")
def getClients(
    name: Optional[str] = None,
    city: Optional[str] = None,
    quantity: int = 20,
):
    df = crear_dataframe_cliente(r"app/archivos/datos.csv", name, city, quantity)

    return df.to_dict(orient="records")