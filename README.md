# ApiPaython

Descripción del desarrollo:
Se desarrollo una aplicación con un API que tiene un método GET para búsqueda en un archivo CSV por querystring de los campos (opcionales) y un parámetro adicional para límite de resultados, al mismo se le pasan dichos parametros en la URL (parametros de búsqueda son por nombre y ciudad y la cantidad se coloca por defecto 20, pero se le pueden especificar los mismos.) 
Ejemplo de query: http://:/search?name=&city=&quantity= 

Se utilizo 
* https://fastapi.tiangolo.com/ (framework)
* Csv (archivo a procesar la información)
* Panda (librería destinada al análisis de datos)
* GitLab (como reporsitorio)
* Docker para el desarrollo local


