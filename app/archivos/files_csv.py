import pandas as pd

def crear_dataframe_cliente(path, name, city, quantity=None):
    df = pd.read_csv(path, sep=",", header=0)
    if name:
        df = df.loc[df["name"] == name]
    if city:
        df = df.loc[df["name"] == city]
    if quantity:
        return df[0:quantity]
    return df